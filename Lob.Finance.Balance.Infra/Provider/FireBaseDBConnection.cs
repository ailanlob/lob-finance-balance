﻿using Google.Cloud.Firestore;
using System;

namespace Lob.Finance.Balance.Infra.Provider
{
    public class FireBaseDBConnection
    {
        public FirestoreDb FireStore
        {
            get
            {
                try
                {
                    return FirestoreDb.Create("lob-finance-balance");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error in access database project {ex.Message}");
                    return null;
                }
            }
        }
    }
}
