﻿namespace Lob.Finance.Balance.Infra.Utils
{
    public static class CollectionsName
    {
        public const string CONFIG_CATEG = "config_category";

        public const string FINANCE_TRANSACTIONS = "finance_transactions";

        public const string STOCK_HISTORIC = "historic";

        public const string STOCK = "stock";

        public const string BLANK = "_blank";

        public const string USER = "users";

        public const string LOG = "log";

        public const string FINANCE_CONSOLIDATED = "finance_consolidated";

        public const string FINANCE_CATEGORY = "finance_category";
    }
}
          