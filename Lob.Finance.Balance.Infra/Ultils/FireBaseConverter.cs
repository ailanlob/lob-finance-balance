﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;

namespace Lob.Finance.Balance.Infra.Utils
{
    public static class FireBaseConverter
    {
        #region ToFireBaseDict
        public static Dictionary<string, object> ToFirebaseDictOne<T>(this T obj)
        {
            Dictionary<string, object> dict = new();
            foreach (var prop in obj.GetType().GetProperties())
            {
                dict.Add(prop.Name, prop.GetValue(obj, null));
            }

            return dict;
        }
        public static Dictionary<string, Dictionary<string, object>> ToFirebaseDict<T>(this List<T> listObj, string keyField = "Key")
        {
            Dictionary<string, Dictionary<string, object>> dictDict = new();

            foreach (var obj in listObj)
            {
                //yield return obj.ToFirebaseDict<T>() // Don't work !!!
                Dictionary<string, object> dict = null;
                //string keyValue = "_U-U_/.\\__KEY__/.\\_U-U_";
                //if (keyField is not null)
                //{
                //    keyValue = obj.GetType().GetProperty(keyField).GetValue(obj, null).GetType().Equals(typeof(DateTime)) ?
                //        DateTime.Parse(obj.GetType().GetProperty(keyField).GetValue(obj, null).ToString()).ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'zzzzzzz") :
                //        obj.GetType().GetProperty(keyField).GetValue(obj, null).ToString();
                //}
                //dictDict.Add(keyValue, obj.ToFirebaseDictOne<T>());

                string key;
                (key, dict) = GetObjKeyValue(obj, keyField);
                dictDict.Add(key, dict);
            }

            return dictDict;
        }

        public static Dictionary<string, Dictionary<string, object>> ToFirebaseDict<T>(this T obj, string keyField = "Key")
        {
            Dictionary<string, Dictionary<string, object>> dictDict = new();
            Dictionary<string, object> dict = null;

            //string keyValue = "_U-U_/.\\__KEY__/.\\_U-U_";
            //if (keyField is not null)
            //{
            //    keyValue = obj.GetType().GetProperty(keyField).GetValue(obj, null).GetType().Equals(typeof(DateTime)) ?
            //        DateTime.Parse(obj.GetType().GetProperty(keyField).GetValue(obj, null).ToString()).ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'zzzzzzz") :
            //        obj.GetType().GetProperty(keyField).GetValue(obj, null).ToString();
            //}
            //dictDict.Add(keyValue, obj.ToFirebaseDictOne<T>());

            string key;
            (key, dict) = GetObjKeyValue(obj, keyField);
            dictDict.Add(key, dict);

            return dictDict;
        }

        private static (string key, Dictionary<string, object> value) GetObjKeyValue(object obj, string keyField = "Key")
        {
            string keyValue = "_U-U_/.\\__KEY__/.\\_U-U_";
            if (keyField is not null)
            {
                keyValue = obj.GetType().GetProperty(keyField).GetValue(obj, null).GetType().Equals(typeof(DateTime)) ?
                    DateTime.Parse(obj.GetType().GetProperty(keyField).GetValue(obj, null).ToString()).ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'zzzzzzz") :
                    obj.GetType().GetProperty(keyField).GetValue(obj, null).ToString();
            }
            return (keyValue, obj.ToFirebaseDictOne<object>());
        }

        #endregion

        #region ToObject
        public static T ToObject<T>(this Dictionary<string, object> dict)
        {
            Type type = typeof(T);
            var obj = Activator.CreateInstance(type);
            try
            {
                foreach (var item in dict)
                {
                    if (item.Value is null)
                        continue;

                    //if (item.Value.GetType().Equals(typeof(double)))
                    //{
                    //    type.GetProperty(item.Key).SetValue(obj, item.Value);
                    //    continue;
                    //}
                    if (item.Value.GetType().Equals(typeof(Google.Cloud.Firestore.Timestamp)))
                    {

                        var teste = (Google.Cloud.Firestore.Timestamp)item.Value;
                        DateTime dateTimeValueParse = teste.ToDateTime().ToLocalTime(); //V2
                        //DateTime dateTimeValueParse = DateTime.ParseExact(item.Value.ToString().Replace("Timestamp:", "").Trim(), "yyyy-MM-ddTHH:mm:ss.ffffffK", null);

                        type.GetProperty(item.Key).SetValue(obj, dateTimeValueParse);
                        continue;
                    }
                    if (item.Value.GetType().Equals(typeof(long)))
                    {
                        if (int.TryParse(item.Value.ToString().Replace(',', '.'), out int intValueParse))
                        {
                            type.GetProperty(item.Key).SetValue(obj, intValueParse);
                            continue;
                        }
                    }
                    try
                    {
                        type.GetProperty(item.Key)?.SetValue(obj, item.Value);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"ERRO in parse {type} field : {item.Key} ");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error in convert Firebase Dict to Object : {ex.Message}");
            }
            return (T)obj;
        }

        public static List<T> ToObject<T>(this Dictionary<string, Dictionary<string, object>> listDict)
        {
            List<T> listObj = new();
            foreach (var dict in listDict)
            {
                listObj.Add(dict.Value.ToObject<T>());
            }

            return listObj;
        }

        public static List<T> ToObject<T>(this IEnumerable<Dictionary<string, object>> listDict)
        {
            List<T> listObj = new();
            foreach (var dict in listDict)
            {
                listObj.Add(dict.ToObject<T>());
            }

            return listObj;
        }
        #endregion

        public static JsonElement ToJson(this IEnumerable<Dictionary<string, object>> dicts)
        {
            string json = "";
            foreach (var dict in dicts)
            {
                var keyValueJson = dict.Select(d => string.Format("\"{0}\": {1}", d.Key, string.Join(",", JsonValueFieldConverter(d.Value))));
                string jsonNode = "{" + string.Join(",", keyValueJson.Select(x => x.Replace(',', '.'))) + "}";
                json += string.Join(",", jsonNode);
            }

            return JsonSerializer.Deserialize<JsonElement>(json);
        }

        public static object JsonValueFieldConverter(object obj)
        {
            Type[] numericTypes = { typeof(int), typeof(long), typeof(double) };

            return numericTypes.Contains(obj.GetType()) ? obj : $"\"{obj}\"";
        }

        public static List<Dictionary<string, object>> FirebaseDocumentToDict(IReadOnlyList<DocumentSnapshot> firebaseDoc)
        {
            List<Dictionary<string, object>> dicts = new();
            foreach (DocumentSnapshot document in firebaseDoc)
            {
                dicts.Add(document.ToDictionary());
            }

            return dicts;
        }
    }
}
