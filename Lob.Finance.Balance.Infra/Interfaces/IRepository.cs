﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Lob.Finance.Balance.Infra.Interfaces
{
    public interface IRepository<T>
    {
        bool Set(List<T> objs);
        bool Set(T obj);

        Task<List<T>> Get();
    }
}
