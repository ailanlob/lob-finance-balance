﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Lob.Finance.Balance.Infra.Interfaces
{
    public interface IRepositoryUser<T>
    {
        bool Set(List<T> objs, string userKey);
        bool Set(T obj, string userKey);
        Task<List<T>> Get(string userKey);
    }
}
