﻿using Google.Cloud.Firestore;
using Lob.Finance.Balance.Infra.Interfaces;
using Lob.Finance.Balance.Infra.Provider;
using Lob.Finance.Balance.Infra.Utils;

namespace Lob.Finance.Balance.Infra.Repositories.Firebase
{
    public class CategoryRepository<T> : FirebaseRepository<T>, IRepositoryUser<T>
    {
        public CategoryRepository(FireBaseDBConnection firebaseConnection) : base(firebaseConnection) { }

        internal override CollectionReference GetCollection(string userKey) => 
            _db.Collection(CollectionsName.USER).Document(userKey).Collection(CollectionsName.FINANCE_CATEGORY);
    }
}
