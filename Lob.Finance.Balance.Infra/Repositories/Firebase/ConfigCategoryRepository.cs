﻿using Google.Cloud.Firestore;
using Lob.Finance.Balance.Infra.Interfaces;
using Lob.Finance.Balance.Infra.Provider;
using Lob.Finance.Balance.Infra.Utils;

namespace Lob.Finance.Balance.Infra.Repositories.Firebase
{
    /// <summary>
    /// Configure category are the repository created from the Transactions informations
    /// </summary>

    public class ConfigCategoryRepository<T> : FirebaseRepository<T>, IRepositoryUser<T>
    {
        public ConfigCategoryRepository(FireBaseDBConnection firebaseConnection) : base(firebaseConnection) { }

        internal override CollectionReference GetCollection(string userKey) => 
            _db.Collection(CollectionsName.USER).Document(userKey).Collection(CollectionsName.CONFIG_CATEG);
    }
}
