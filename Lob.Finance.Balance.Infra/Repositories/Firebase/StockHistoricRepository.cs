﻿using Google.Cloud.Firestore;
using Lob.Finance.Balance.Infra.Interfaces;
using Lob.Finance.Balance.Infra.Provider;
using Lob.Finance.Balance.Infra.Utils;

namespace Lob.Finance.Balance.Infra.Repositories.Firebase
{
    public class StockHistoricRepository<T> : FirebaseRepository<T>, IRepository<T>
    {
        public StockHistoricRepository(FireBaseDBConnection firebaseConnection) : base(firebaseConnection) { }

        internal override CollectionReference GetCollection(string userKey) => 
            _db.Collection(CollectionsName.STOCK).Document(userKey).Collection(CollectionsName.STOCK_HISTORIC);
    }
}
