﻿using Google.Cloud.Firestore;
using Lob.Finance.Balance.Infra.Interfaces;
using Lob.Finance.Balance.Infra.Provider;
using Lob.Finance.Balance.Infra.Utils;

namespace Lob.Finance.Balance.Infra.Repositories.Firebase
{
    public class LogRepository<T> : FirebaseRepository<T>, IRepository<T>
    {
        public LogRepository(FireBaseDBConnection firebaseConnection) : base(firebaseConnection) { }

        internal override CollectionReference GetCollection(string userKey) => 
            _db.Collection(CollectionsName.USER).Document(userKey).Collection(CollectionsName.LOG);
    }
}
