﻿using Google.Cloud.Firestore;
using Lob.Finance.Balance.Infra.Interfaces;
using Lob.Finance.Balance.Infra.Provider;
using Lob.Finance.Balance.Infra.Utils;

namespace Lob.Finance.Balance.Infra.Repositories.Firebase
{
    public class StockRepository<T> : FirebaseRepository<T>, IRepository<T>
    {
        public StockRepository(FireBaseDBConnection firebaseConnection) : base(firebaseConnection) { }

        internal override CollectionReference GetCollection() => _db.Collection(CollectionsName.STOCK);
    }
}
