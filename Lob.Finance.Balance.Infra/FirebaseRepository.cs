﻿using Google.Cloud.Firestore;
using Lob.Finance.Balance.Infra.Interfaces;
using Lob.Finance.Balance.Infra.Provider;
using Lob.Finance.Balance.Infra.Utils;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Lob.Finance.Balance.Infra
{
    public abstract class FirebaseRepository<T> : IRepository<T>, IRepositoryUser<T>
    {
        internal static FirestoreDb _db;
        private readonly FireBaseDBConnection _firebaseConnection;

        public FirebaseRepository(FireBaseDBConnection firebaseConnection)
        {
            _firebaseConnection = new();
            try
            {
                _db = _firebaseConnection.FireStore;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error in access database project [{typeof(T)}] {ex.Message}");
            }
        }

        #region SET
        private bool Set(Dictionary<string, Dictionary<string, object>> dict)
        {
            foreach (var config in dict)
            {
                if (GetCollection().Document(config.Key.ToLower()).SetAsync(config.Value).GetAwaiter().GetResult() == null)
                {
                    Console.WriteLine($"Fail in insert {config.Key}");
                    return false;
                }
            }

            return true;
        }
        private bool Set(Dictionary<string, Dictionary<string, object>> dict, string userKey)
        {
            foreach (var config in dict)
            {
                if (GetCollection(userKey).Document(config.Key.ToLower()).SetAsync(config.Value).GetAwaiter().GetResult() == null)
                {
                    Console.WriteLine($"Fail in insert {config.Key}");
                    return false;
                }
            }

            return true;
        }

        public bool Set(List<T> objs) => Set(objs.ToFirebaseDict());
        public bool Set(List<T> objs, string userKey) => Set(objs.ToFirebaseDict(), userKey);

        public bool Set(T obj) => Set(obj.ToFirebaseDict());
        public bool Set(T obj, string userKey) => Set(obj.ToFirebaseDict(), userKey);
        #endregion

        public bool Update(Dictionary<string, Dictionary<string, object>> dict)
        {
            foreach (var config in dict)
            {
                if (GetCollection().Document(config.Key.ToLower()).UpdateAsync(config.Value).GetAwaiter().GetResult() == null)
                {
                    Console.WriteLine($"Fail in insert {config.Key}");
                    return false;
                }
            }

            return true;
        }

        #region GET
        private async Task<IEnumerable<Dictionary<string, object>>> GetFirebase() =>
            FirebaseDocumentToDict(GetCollection().GetSnapshotAsync().GetAwaiter().GetResult());
        private async Task<IEnumerable<Dictionary<string, object>>> GetFirebase(string userKey)
        {
            try
            {
                var result = GetCollection(userKey);
                var result2 = result.GetSnapshotAsync().GetAwaiter().GetResult();
                return FirebaseDocumentToDict(result2);
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERRO AQUI1: " + ex.Message);
                return null;
            }
        }

        public async Task<List<T>> Get()
        {
            var firebaseReturn = await GetFirebase();
            return firebaseReturn.ToObject<T>();
        }

        public async Task<List<T>> Get(string userKey)
        {
            try
            {
                var firebaseReturn = await GetFirebase(userKey);
                return firebaseReturn.ToObject<T>();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        private static List<Dictionary<string, object>> FirebaseDocumentToDict(IReadOnlyList<DocumentSnapshot> firebaseDoc)
        {
            List<Dictionary<string, object>> dicts = new();
            foreach (DocumentSnapshot document in firebaseDoc)
            {
                dicts.Add(document.ToDictionary());
            }

            return dicts;
        }
        #endregion

        internal virtual CollectionReference GetCollection() => _db.Collection(CollectionsName.BLANK);
        internal virtual CollectionReference GetCollection(string _key) => _db.Collection(CollectionsName.BLANK);
    }
}
