﻿//using Google.Cloud.Firestore;
//using Lob.Finance.Balance.Infra.Utils;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text.Json;
//using System.Threading.Tasks;

//namespace Lob.Finance.Balance.Infra
//{
//    public abstract class GenericRepository<T>     // : IGenericRepository
//    {
//        public static FirestoreDb _db;
//        public static string _docKey;

//        public GenericRepository(string docKey = "root")
//        {
//            try
//            {
//                _db = FirestoreDb.Create("lob-finance-balance");
//            }
//            catch (Exception ex)
//            {
//                Console.WriteLine($"Error in access database project {ex.Message}");
//            }
//            _docKey = docKey;
//        }

//        public bool Set(Dictionary<string, Dictionary<string, object>> dict)
//        {
//            foreach (var config in dict)
//            {
//                if (GetCollection().Document(config.Key.ToLower()).SetAsync(config.Value).GetAwaiter().GetResult() == null)
//                {
//                    Console.WriteLine($"Fail in insert {config.Key}");
//                    return false;
//                }
//            }

//            return true;
//        }

//        public bool Update(Dictionary<string, Dictionary<string, object>> dict)
//        {
//            foreach (var config in dict)
//            {
//                if (GetCollection().Document(config.Key.ToLower()).UpdateAsync(config.Value).GetAwaiter().GetResult() == null)
//                {
//                    Console.WriteLine($"Fail in insert {config.Key}");
//                    return false;
//                }
//            }

//            return true;
//        }
//        //REMOVE USE OF T
//        public async Task<List<T>> Get()
//        {
//            QuerySnapshot snapshot = await GetCollection().GetSnapshotAsync();

//            Dictionary<string, Dictionary<string, object>> dict = new();
//            foreach (DocumentSnapshot document in snapshot.Documents)
//            {
//                dict.Add(document.Id, document.ToDictionary());
//            }

//            return dict.Count > 0 ? dict.ToObject<T>() : null;
//        }

//        public virtual CollectionReference GetCollection()
//        {
//            return _db.Collection(CollectionsName.BLANK);
//        }
//    }
//}
