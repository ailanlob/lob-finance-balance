using Lob.Finance.Balance.Domain;
using Lob.Finance.Balance.Domain.Extensions;
using Lob.Finance.Balance.Domain.Models;
using Lob.Finance.Balance.Domain.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Lob.Finance.Balance
{
    public class Program
    {
        static void Main(string[] args)
        {
            var serviceProvider = new ServiceCollection()
                .AddLogging()
                .AddSingleton<ConsoleApp>()
                .AddSingleton<ImportService>()
                .RegisterDomain()
                .RegisterInfra()
                .BuildServiceProvider();

            serviceProvider.GetService<ConsoleApp>().Run(args);
        }
    }
}


