﻿using Lob.Finance.Balance.Domain.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace Lob.Finance.Balance.Domain.Services
{
    public class ImportService
    {
        public List<TransactionsModel> ReadJsonTransactions() => JsonSerializer.Deserialize<List<TransactionsModel>>(ReadJson("base.json"));

        public List<ConfigCategoryModel> ReadJsonCategoryConfig() => JsonSerializer.Deserialize<List<ConfigCategoryModel>>(ReadJson("category.json"));

        public static string ReadJson(string jsonName)
        {
            using (StreamReader reader = new("category.json"))
            {
                return reader.ReadToEnd();
            }
        }

        public List<TransactionsModel> ReadCSVTransactions()
        {
            //TODO: Import CSV from the CEI
            List<string[]> readsList = new();
            string[] readHeader = Array.Empty<string>();
            using (StreamReader reader = new("finance-history.csv"))
            {
                string line = reader.ReadLine();
                readHeader = line is null ? Array.Empty<string>() : line.Split(';', StringSplitOptions.RemoveEmptyEntries);
                while ((line = reader.ReadLine()) is not null)
                {
                    //readsList.Add(line.Replace(',', '.').Split(';', StringSplitOptions.RemoveEmptyEntries));
                    readsList.Add(line.Split(';', StringSplitOptions.RemoveEmptyEntries));
                }
            }

            try
            {
                var result = JsonSerializer.Deserialize<List<TransactionsModel>>(JsonSerializer.Serialize(ArrayToDictionary(readHeader, readsList)));
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }

            //return JsonSerializer.Deserialize<List<TransactionsModel>>(JsonSerializer.Serialize(ArrayToDictionary(readHeader, readsList)));
        }

        public static List<Dictionary<string, object>> ArrayToDictionary(string[] readHeader, List<string[]> readsList)
        {
            List<Dictionary<string, object>> listObjResult = new();
            foreach (var item in readsList)
            {
                Dictionary<string, object> objResult = new();
                for (var i = 0; i < readHeader.Length; i++)
                {
                    objResult.Add(readHeader[i], item[i]);
                }
                listObjResult.Add(objResult);
            }
            return listObjResult;
        }
    }
}
