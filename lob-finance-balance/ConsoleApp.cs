﻿using Lob.Finance.Balance.Domain;
using Lob.Finance.Balance.Domain.Models;
using Lob.Finance.Balance.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lob.Finance.Balance
{
    public class ConsoleApp
    {
        private static List<ConfigCategoryModel> _configCategory;
        private static List<ConsolidatedModel> _consolidated;
        private static List<CategoryModel> _category;
        private static ResumeModel _resume;
        private static List<TransactionsModel> _transactions;

        private static IService<ResumeModel> _resumeService;
        private static IService<CategoryModel> _categoryService;
        private static IService<ConfigCategoryModel> _configCategService;
        private static IService<TransactionsModel> _transactionsService;
        private static IService<StockModel> _stockService;
        private static IService<StockHistoricModel> _stockHistoricService;
        private static IService<ConsolidatedModel> _consolidatedService;

        private static InformationMining _informationMining;
        private static BalanceService _balance;
        private static UserService _userService;
        private static ImportService _importService;

        private static bool _continue = true;

        public ConsoleApp(
            IService<ResumeModel> resumeService,
            IService<CategoryModel> categoryService,
            IService<ConfigCategoryModel> configCategoryService,
            IService<TransactionsModel> transactionsService,
            IService<StockModel> stockService,
            IService<StockHistoricModel> stockHistoricService,
            IService<ConsolidatedModel> consolidatedService,
            BalanceService balance,
            InformationMining informationMining,
            UserService userService,
            ImportService importService
            )
        {
            _resumeService = resumeService;
            _categoryService = categoryService;
            _configCategService = configCategoryService;
            _transactionsService = transactionsService;
            _stockService = stockService;
            _stockHistoricService = stockHistoricService;
            _consolidatedService = consolidatedService;
            _balance = balance;
            _informationMining = informationMining;
            _userService = userService;
            _importService = importService;
        }

        private static async Task<Dictionary<string, Action>> MenuOptionsDescription() => new Dictionary<string, Action>()
            {
                //Menu
                { "", async () => await PrintMenu() },
                { "/?", async () => await PrintMenu() },
                { "?", async () => await PrintMenu() },
                { "HELP;Menu options;n", async () => await PrintMenu() },
                //Balance
                { "B;Balance transactions", async () => _consolidated = await _balance.StockValueToBeInvested(true) },
                { "R;Reset Balance", () => _consolidated = _balance.ResetStockValueToBeInvested() },
                { "A;Incress the invested value", async () => await ChangeToInvestedValue() },
                { "ADD -STOCK;Include a New Stock;n", AddStock },
                //Print
                { "I;Print all", PrintFull },
                { "I -CAT;Print categories transactions", () => PrintCategory(true) },
                { "I -CON;Print consolidated transactions", () => PrintConsolidated(null) },
                { "I -RES;Print finance resume;n", PrintResume },
                //Clear
                { "L;Clear;n", Console.Clear },
                //Generate Category
                { "G -CAT;Generate category consolidated;n", async () => _category = await _categoryService.GenerateList() },
                //EXIT
                { "X;EXIT", () => _continue = false }
            };

        public async Task Run(string[] args)
        {
            UserModel user = new();
            if (args.Length == 0)
            {
                Console.WriteLine("User: ");
                user.Name = Console.ReadLine();
                Console.WriteLine("Password: ");
                user.Password = Console.ReadLine();
            }
            else
            {
                user.Name = args[0];
                user.Password = args[1];
            }

            _userService.Login("root_new", user.Password);

            #region TESTE
            #endregion

            Console.WriteLine($"Balance Logic! {user.Name}");

            #region [OK]GetHistoricPrice
            //Stock stockTest = new()
            //{
            //    Name = "ABEV3",
            //    Price = 0.0,
            //    TradingView = "ambev-s-a-on-ABEV3"
            //};
            //double priceTest = GetStockHistoricPrice(stockTest, new DateTime(2021, 04, 19));
            #endregion

            #region [OK]CategoryConfig
            //TODO:[CONSOLE-APP] Edit category config in console
            //_configCategory = JsonSerializer.Deserialize<ConfigCategory>(_configCategRepository.Get().GetAwaiter().GetResult());   // . _configCategRepository.Get().GetAwaiter().GetResult();
            _configCategory = await _configCategService.GetList();
            if (_configCategory is null)
            {
                _configCategory ??= _importService.ReadJsonCategoryConfig();
                _configCategService.Set(_configCategory);
            }
            #endregion

            #region [OK]FinanceTransactions
            //TODO: Create a csv Version and compare with the database registers to not Update the database
            //_transactions = await _transactionsService.GetList();
            if (_transactions is null)
            {
                _transactions ??= _importService.ReadCSVTransactions();
                _transactionsService.Set(_transactions);
            }
            #endregion

            #region [OK]Stocks
            List<StockModel> stocksRead = await _stockService.GetList() ?? new();
            //_stockService.InsertNewsFromTransactionsAsync(_transactions).GetAwaiter().GetResult();
            #endregion
            if (1 == 2)
            {
                #region [OK]StockPrices
                bool hasUpdatedPrice = false;
                List<StockModel> stocksReadToPrice = await _stockService.GetList() ?? new();
                try
                {
                    foreach (var stock in stocksReadToPrice)
                    {
                        //List<StockHistoric> stockHistoric = new();
                        (double price, double dailyYield) = stock.TradingView.Length > 0 ? _informationMining.GetStockPriceAndYield(stock.TradingView).GetAwaiter().GetResult() : (0.0, 0.0);
                        if (price > 0)
                        {
                            //stockHistoric.Add(new() { Price = price, Name = stock.Name });
                            //_stockHistoricRepository.Set(stockHistoric.ToFirebaseDict("UpdatedDate"), stock.Name.ToLower());

                            stock.Price = price;
                            stock.DailyYield = dailyYield;
                            stock.UpdatedDate = DateTime.UtcNow;

                            hasUpdatedPrice = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    PrintLnError($"stocksReadToPrice: {ex.Message}");
                }
                if (hasUpdatedPrice)
                    _stockService.Set(stocksReadToPrice);
                #endregion
            }

            #region [OK]FinanceConsolidated
            _consolidated = await _consolidatedService.GenerateList();
            _consolidatedService.Set(_consolidated);
            #endregion

            #region [OK]Resume
            _resume = await _resumeService.Generate();
            _resumeService.Set(_resume);
            #endregion

            #region [OK]FinanceCategory
            _category = await _categoryService.GenerateList();
            _categoryService.Set(_category);
            #endregion

            //_balance = new(_category, _consolidated, _resume);

            var menuOptions = new Dictionary<string, Action>();
            foreach (var item in await MenuOptionsDescription())
            {
                menuOptions.Add(item.Key.Split(';')[0], item.Value);
            }
            while (_continue)
            {
                Console.WriteLine("");
                Console.Write("LOB:\\Finance\\Balance> ");
                string command = Console.ReadLine().ToUpper();
                if (menuOptions.ContainsKey(command))
                    menuOptions[command].Invoke();
                else
                    PrintLnError($"Invalid command {command}");
            }
            Console.WriteLine("FIM");
        }

        private static async Task ChangeToInvestedValue()
        {
            Console.Write($"Inset the value to be invested [current {_resume.NeedsToBeInvested}]: ");
            if (double.TryParse(Console.ReadLine(), out double totalInvestedAux))
            {
                _resume.NeedsToBeInvested = totalInvestedAux;
                _resumeService.Set(_resume);
                _category = await _categoryService.GenerateList();
                //_balance = new(_category, _consolidated, _resume);
            }
            else
            {
                PrintLnError($"Invalid double format value informed");
            }
        }

        private static void AddStock()
        {
            Console.Write("Stock Abbreviation: ");
            string name = Console.ReadLine();
            Console.Write("Stock scraping: ");
            string tradingView = Console.ReadLine();

            Console.WriteLine($"Confirm the scraping link: \n {_informationMining.GetScrapingPage(tradingView)}");
            Console.Write("(Y)es or (N)ot :");
            if (Console.ReadLine().ToLower() == "y")
            {
                (double price, double dailyYield) = _informationMining.GetStockPriceAndYield(tradingView).GetAwaiter().GetResult();
                if (price == 0)
                {
                    PrintLnError("Fail in get price");
                    return;
                }
                StockModel stockModel = new()
                {
                    Name = name.ToUpper(),
                    TradingView = tradingView,
                    Price = price,
                    DailyYield = dailyYield
            };
                _stockService.Set(stockModel);
            }
        }

        private static async Task PrintMenu()
        {
            Console.ResetColor();
            Console.WriteLine($"{new string('-', 30)}COMMANDS{new string('-', 30)}");
            foreach (var item in await MenuOptionsDescription())
            {
                var keySplit = item.Key.Split(';');
                int spaceLenght = 30 - keySplit[0].Length;

                if (keySplit.Length == 1)
                    Console.WriteLine(keySplit[0].ToLower());

                if (keySplit.Length > 1)
                    Console.WriteLine($"{keySplit[0].ToLower()}{new string(' ', spaceLenght)}{keySplit[1]}");

                if (keySplit.Length == 3)
                    Console.WriteLine("");
            }
        }

        public static void PrintFull()
        {
            foreach (CategoryModel finance in _category)
            {
                Console.WriteLine($"{"Name",-10} | {"Total",8} | {"%",5} | {"Config %",10} | {"Dif %",5} | {"Dif Sts"} | {"Need R$",5}");
                string values = $"{finance.Name,-10} | " +
                    $"{finance.TotalValue,8:0.00} | " +
                    $"{finance.CurrentPercent,5:0.00} | " +
                    $"{finance.TargetPercent,10:0.00} | " +
                    $"{finance.DifferPercent,5:0.00} | " +
                    $"{(finance.DifferPercent > 1 ? "OVER" : finance.DifferPercent < (1 * -1) ? "BELOW" : "OK"),7} |" +
                    $"{(finance.NeedsToBeInvested),5:0.00}";

                if (finance.YieldValue >= 0)
                    PrintLnSucess(values);
                else
                    PrintLnError(values);

                PrintConsolidated(_consolidated.Where(x => x.Category == finance.Name).ToList());
            }

            PrintResume();
        }

        public static void PrintResume()
        {
            Console.WriteLine("");
            Console.WriteLine($"Total In Stock =  {_category.Sum(x => x.TotalValue):C0}");
            Console.WriteLine($"Total Invested =  { _resume.TotalInvestedValue:C0}");
            Console.WriteLine($"Total Yield =  { _resume.YieldValue:C0}");
            Console.WriteLine($"Total To Be Invested =  { (_consolidated.Sum(x => x.NeedsToBePurchaseAmount)):C0}");
            Console.WriteLine($"Total Need Balance =  { _resume.NeedsToBeInvested:C0}");
            Console.WriteLine("");
        }

        public static void PrintConsolidated(List<ConsolidatedModel> listFinanceBase)
        {
            listFinanceBase ??= _consolidated;
            double totalInvested = listFinanceBase.Sum(x => x.PricePaid);

            Console.WriteLine($"{"   Name",-10} | {"PaidPrice",11} | {"TotalPrice",11} | {"MarketPrice",11} | {"Amount",8} | {"Need Amount",10} | {"Need Price",10} | {"Yield %",10} | {"Yield $",5}");
            foreach (ConsolidatedModel finance in listFinanceBase)
            {
                string values = ($"   {finance.Abbreviation,-7} | " +
                    $"   {finance.PricePaid,8:0.00} | " +
                    $"   {(finance.MarketPrice == 0 ? 0.0 : finance.MarketPrice * finance.Amount),8:0.00} | " +
                    $"   {finance.MarketPrice,8} | " +
                    $"   {finance.Amount,5} | " +
                    $"   {finance.NeedsToBePurchaseAmount,8} | " +
                    $"   {(finance.NeedsToBePurchaseAmount * finance.MarketPrice),7:C0} | " +
                    $"   {finance.YieldPercent,7:0.00} | " +
                    $"   {finance.YieldValue,5:0.00}");

                if (finance.YieldValue >= 0)
                    PrintLnSucess(values);
                else
                    PrintLnError(values);
            }
            Console.WriteLine();
        }

        public static void PrintLnError(string msg) => PrintLnColor(msg, ConsoleColor.Red);

        public static void PrintLnSucess(string msg) => PrintLnColor(msg, ConsoleColor.Green);

        public static void PrintLnColor(string msg, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(msg);
            Console.ResetColor();
        }

        public static void PrintLogo()
        {
            Console.WriteLine(new string('$', 10));
        }

        public static void PrintCategory(bool printMenuOneTime = false)
        {
            string menu = $"{"Name",-10} | {"Total",8} | {"%",5} | {"Config %",10} | {"Dif %",5} | {"Dif Sts"} | {"Need R$",5}";
            if (printMenuOneTime)
                Console.WriteLine(menu);

            foreach (CategoryModel finance in _category)
            {
                if (!printMenuOneTime)
                    Console.WriteLine(menu);
                string values = $"{finance.Name,-10} | " +
                    $"{finance.TotalValue,8:0.00} | " +
                    $"{finance.CurrentPercent,5:0.00} | " +
                    $"{finance.TargetPercent,10:0.00} | " +
                    $"{finance.DifferPercent,5:0.00} | " +
                    $"{(finance.DifferPercent > 1 ? "OVER" : finance.DifferPercent < (1 * -1) ? "BELOW" : "OK"),7} |" +
                    $"{(finance.NeedsToBeInvested),5:0.00}";

                if (finance.YieldValue >= 0)
                    PrintLnSucess(values);
                else
                    PrintLnError(values);
            }
            Console.WriteLine();
        }
    }
}
