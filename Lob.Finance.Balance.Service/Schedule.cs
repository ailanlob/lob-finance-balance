﻿using System;

namespace Lob.Finance.Balance.Service
{
    public class Schedule
    {
        public DateTime NextExecutionTime { get; private set; }

        public Schedule(TimeSpan time) => UpdateNextExecution(time);

        public Schedule() { }

        public bool ItsTime() => NextExecutionTime <= DateTime.Now;

        public void UpdateNextExecution(TimeSpan time) => NextExecutionTime = DateTime.Now.Add(time);
        public void UpdateNextExecution(DateTime datetime) => NextExecutionTime = datetime;
    }
}
