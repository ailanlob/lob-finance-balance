﻿using Lob.Finance.Balance.Domain;
using Lob.Finance.Balance.Domain.Models;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Lob.Finance.Balance.Service
{
    internal class Worker : BackgroundService
    {
        private readonly ILogger _logger;
        private static readonly List<DayOfWeek> _validDayOfWeek = new() { DayOfWeek.Monday, DayOfWeek.Tuesday, DayOfWeek.Wednesday, DayOfWeek.Thursday, DayOfWeek.Friday };

        private readonly Schedule _schedule = new();
        private readonly int _intervalInMiliseconds = (int)TimeSpan.FromMinutes(30).TotalMilliseconds;
        private readonly List<int> executionHours = new() { 10, 14, 18 };

        private List<StockModel> _stocks;
        private readonly IService<StockModel> _stockService;

        public Worker(
            ILogger<Worker> logger,
            IService<StockModel> stockService)
        {
            _logger = logger;
            _stockService = stockService;
            _stocks = _stockService.GetList().GetAwaiter().GetResult();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("SendingPrinterInfo::StartAsync ");
            return base.StartAsync(cancellationToken);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("OnStarted has been called.");
            InformationMining mining = new();
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    Console.WriteLine($"[Timer: {_schedule.NextExecutionTime:dd/MM/yyyy HH:mm:ss} - DateTime.UtcNow: {DateTime.Now:dd/MM/yyyy HH:mm:ss}]");
                    if (_schedule.ItsTime())
                    {
                        Console.WriteLine("Is Time");

                        List<StockHistoricModel> stockHistorics = new();
                        foreach (var stock in _stocks)
                        {
                            (double price, double dailyYield) = await mining.GetStockPriceAndYield(stock.TradingView);
                            stock.Price = price > 0 ? price : stock.Price;
                            stock.DailyYield = dailyYield > 0 ? dailyYield : stock.DailyYield;
                            stock.UpdatedDate = DateTime.UtcNow;
                            stockHistorics.Add(new() { Price = price, Name = stock.Name, UpdatedDateTime = DateTime.UtcNow });
                        }

                        Console.WriteLine($"Update stocks price [{_stockService.Set(_stocks)}]");

                        NextExecution();
                        Console.WriteLine($"Next execution : [{_schedule.NextExecutionTime:dd/MM/yyyy HH:mm:ss}]");
                    }
                }
                catch(Exception ex)
                {
                    _logger.LogError("WorkterERRO : " + ex.Message);
                }
                await Task.Delay(_intervalInMiliseconds, stoppingToken);
            }
        }

        private void NextExecution()
        {
            var executionHour = executionHours.Find(x => x > DateTime.Now.Hour);
            if (executionHour == 0)
            {
                _schedule.UpdateNextExecution(NextDayExecution(DateTime.Today.AddDays(1)).AddHours(executionHours[0]));
                return;
            }

            _schedule.UpdateNextExecution(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, executionHour, 0, 0));
        }

        private DateTime NextDayExecution(DateTime nextDateReceiver) => _validDayOfWeek.Contains(nextDateReceiver.DayOfWeek) ? nextDateReceiver : NextDayExecution(nextDateReceiver.AddDays(1));
    }
}