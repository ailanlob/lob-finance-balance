﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Lob.Finance.Balance.Domain
{
    public interface IService<T>
    {
        Task<T> Get();
        Task<List<T>> GetList();

        bool Set(T model);
        bool Set(List<T> models);

        Task<T> Generate();
        Task<List<T>> GenerateList();
    }
}
