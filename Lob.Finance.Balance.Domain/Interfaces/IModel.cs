﻿namespace Lob.Finance.Balance.Domain.Interfaces
{
    public interface IModel
    {
        string Key { get; }
    }
}
