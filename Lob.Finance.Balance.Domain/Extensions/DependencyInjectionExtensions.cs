﻿using Lob.Finance.Balance.Domain.Models;
using Lob.Finance.Balance.Domain.Services;
using Lob.Finance.Balance.Infra.Interfaces;
using Lob.Finance.Balance.Infra.Provider;
using Lob.Finance.Balance.Infra.Repositories.Firebase;
using Microsoft.Extensions.DependencyInjection;

namespace Lob.Finance.Balance.Domain.Extensions
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection RegisterDomain(this IServiceCollection services)
        {
            services.AddSingleton<BalanceService>();
            services.AddSingleton<UserService>();
            services.AddSingleton<InformationMining>();

            services.AddTransient<IService<CategoryModel>, CategoryService>();
            services.AddTransient<IService<ConfigCategoryModel>, ConfigCategoryService>();
            services.AddTransient<IService<ConsolidatedModel>, ConsolidatedService>();
            services.AddTransient<IService<ResumeModel>, ResumeService>();
            services.AddTransient<IService<StockHistoricModel>, StockHistoricService>();
            services.AddTransient<IService<StockModel>, StockService>();
            services.AddTransient<IService<TransactionsModel>, TransactionsService>();
            return services;
        }
        public static IServiceCollection RegisterInfra(this IServiceCollection services)
        {
            services.AddTransient<IRepositoryUser<CategoryModel>,CategoryRepository<CategoryModel>>();
            services.AddTransient<IRepositoryUser<ConfigCategoryModel>,ConfigCategoryRepository<ConfigCategoryModel>>();
            services.AddTransient<IRepositoryUser<ConsolidatedModel>,ConsolidatedRepository<ConsolidatedModel>>();
            services.AddTransient<IRepositoryUser<TransactionsModel>, TransactionsRepository<TransactionsModel>>();
                     
            services.AddTransient<IRepository<ResumeModel>, ResumeRepository<ResumeModel>>();
            services.AddTransient<IRepository<StockHistoricModel>, StockHistoricRepository<StockHistoricModel>>();
            services.AddTransient<IRepository<StockModel>, StockRepository<StockModel>>();

            services.AddTransient<FireBaseDBConnection>();
            //services.AddSingleton<LogRepository<LogModel>();
            return services;
        }


    }
}
