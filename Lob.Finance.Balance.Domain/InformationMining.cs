﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Lob.Finance.Balance.Domain
{
    public class InformationMining
    {
        private const string _scrapingPage = @"https://br.advfn.com/bolsa-de-valores/bovespa/";

        private async Task<HtmlAgilityPack.HtmlDocument> GetHtmlPage(string urlAddress)
        {
            string result = "";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urlAddress);

            request.UseDefaultCredentials = true;
            request.Proxy.Credentials = CredentialCache.DefaultCredentials;
            HttpWebResponse response;
            try
            {
                response = (HttpWebResponse)await request.GetResponseAsync();
            }
            catch
            {

                //failMessage = $"[ERRO] GetHtmlPage {ex.Message}";
                return new HtmlAgilityPack.HtmlDocument();
            }

            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = string.IsNullOrWhiteSpace(response.CharacterSet) ? 
                        new StreamReader(receiveStream) :
                        new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet));

                result = readStream.ReadToEnd();

                response.Close();
                readStream.Close();
            }

            var htmlDoc = new HtmlAgilityPack.HtmlDocument();
            htmlDoc.LoadHtml(result);

            return htmlDoc;
        }

        public async Task<(double price, double dailyYield)> GetStockPriceAndYield(string stock)
        {
            try
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"Searching Price {stock}");
                HtmlAgilityPack.HtmlDocument page = await GetHtmlPage(GetScrapingPage(stock));

                var auxNode = page.DocumentNode.SelectNodes("//span[@class='qs-current-price']//span");
                var price = auxNode.SelectMany(y => y.Attributes.Where(x => x.Name == "id" && x.Value.Contains("quoteElementPiece1"))).FirstOrDefault().OwnerNode.InnerHtml;

                auxNode = page.DocumentNode.SelectNodes("//div[@class='price-info']//span");
                var dailyYield = auxNode.SelectMany(y => y.Attributes.Where(x => x.Name == "id" && x.Value.Contains("quoteElementPiece2"))).FirstOrDefault().OwnerNode.InnerHtml;

                Console.WriteLine($"Price of {stock} find {price} with Yield {dailyYield}");
                Console.ResetColor();

                return (double.TryParse(price, out double resultPrice) ? resultPrice : 0.0,
                        double.TryParse(dailyYield, out double resultDailyYield) ? resultDailyYield : 0.0);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"GetStockPriceAndYield: {ex.Message}");
                return (0.0, 0.0);
            }
        }

        public async Task<double> GetStockPriceDate(string stock, DateTime date)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Buscando Preço {stock}");

            string dateStr = date.ToString("dd/MM/yy");
            HtmlAgilityPack.HtmlDocument page = await GetHtmlPage(@"https://br.advfn.com/bolsa-de-valores/bovespa/" + stock + "/historico/mais-dados-historicos?current=0&Date1=" + dateStr + "&Date2=" + dateStr);
            //https://br.advfn.com/bolsa-de-valores/bovespa/ambev-s-a-on-ABEV3/historico/mais-dados-historicos?current=0&Date1=01/02/21&Date2=01/02/21
            var teste = page.DocumentNode.SelectNodes("//td[@class='Numeric']");
            var price = teste.FirstOrDefault().InnerHtml;

            Console.WriteLine($"Preço {stock} encontrado {price}");
            Console.ResetColor();

            return double.TryParse(price, out double result) ? result : 0.0;
        }

        public string GetScrapingPage(string stockPage) => _scrapingPage + stockPage + @"/cotacao";
    }
}
