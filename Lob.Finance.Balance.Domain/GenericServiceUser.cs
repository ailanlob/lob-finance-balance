﻿using Lob.Finance.Balance.Domain.Services;
using Lob.Finance.Balance.Infra.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lob.Finance.Balance.Domain
{
    public abstract class GenericServiceUser<T> : IService<T>
    {
        private UserService _user { get; set; }

        public string _userKey => _user.UserKey;

        private IRepositoryUser<T> _repository;

        public GenericServiceUser(UserService user, IRepositoryUser<T> repository)
        {
            _user = user;
            _repository = repository;

            //_userKey = _user.UserKey;
        }

        public async Task<T> Get()
        {
            var result = await _repository.Get(_userKey);
            return result.FirstOrDefault();
        }

        public async Task<List<T>> GetList() => await _repository.Get(_userKey);

        public bool Set(T model) => _repository.Set(model, _userKey);

        public bool Set(List<T> models) => _repository.Set(models, _userKey);

        public virtual async Task<List<T>> GenerateList() => throw new System.NotImplementedException();

        public virtual async Task<T> Generate() => throw new System.NotImplementedException();
    }
}
