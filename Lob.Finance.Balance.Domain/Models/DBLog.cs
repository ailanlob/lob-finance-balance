﻿using Lob.Finance.Balance.Domain.Interfaces;
using System;

namespace Lob.Finance.Balance.Domain.Models
{
    public class DBLog : IModel
    {
        public string Key => $"{Level}-{InsertDate:yyyy-MM-dd}";

        public string Message { get; set; }

        public DateTime InsertDate { get; set; } = DateTime.UtcNow; //Use only yyyy-MM-dd

        public int Level { get; set; }


    }
}
