﻿namespace Lob.Finance.Balance.Domain.Models
{
    public class UserModel
    {
        ///<summary>
        /// User Name for acess the user informations
        ///</summary>
        public string Name { get; set; }

        /// <summary>
        /// Password string view format received from front-end
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Criptografed password, saved in the database
        /// </summary>
        public string Secrect { get; set; }
    }
}
