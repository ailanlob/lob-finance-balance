﻿using Lob.Finance.Balance.Domain.Interfaces;

namespace Lob.Finance.Balance.Domain.Models
{
    public class CategoryModel : IModel
    {
        public string Key => Name;
        public string Name { get; set; }
        public double TargetPercent { get; set; }
        public double CurrentPercent { get; set; }
        public double DifferPercent { get; set; }
        public double TotalValue { get; set; }
        public double NeedsToBeInvested { get; set; }
        public double YieldValue { get; set; }
        public double YieldPercent { get; set; }
    }
}
