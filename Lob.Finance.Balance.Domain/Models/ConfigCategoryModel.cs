﻿using Lob.Finance.Balance.Domain.Interfaces;

namespace Lob.Finance.Balance.Domain.Models
{
    public class ConfigCategoryModel : IModel
    {
        public string Key => Name;
        public string Name { get; set; }
        public double TargetPercent { get; set; } = 0;
        public string ConfigureDate { get; set; }
    }
}
