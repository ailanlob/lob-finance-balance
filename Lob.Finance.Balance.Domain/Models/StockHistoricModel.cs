﻿using Lob.Finance.Balance.Domain.Interfaces;
using System;

namespace Lob.Finance.Balance.Domain.Models
{
    public class StockHistoricModel : IModel
    {
        public string Key => UpdatedDate;
        public string Name { get; set; }
        public double Price { get; set; }
        public string UpdatedDate { get; set; } = DateTime.UtcNow.ToString("yyyy-MM-dd");
        public DateTime UpdatedDateTime { get; set; } = DateTime.UtcNow;
    }
}