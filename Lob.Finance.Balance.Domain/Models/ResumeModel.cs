﻿using Lob.Finance.Balance.Domain.Interfaces;

namespace Lob.Finance.Balance.Domain.Models
{
    public class ResumeModel : IModel
    {
        public string Key => User;
        public string User { get; set; }
        public double TotalInvestedValue { get; set; }
        public double NeedsToBeInvested { get; set; }
        public double YieldValue { get; set; }
        public double YieldPercent { get; set; }
        public double CurrentTotalValue { get; set; }
    }
}
