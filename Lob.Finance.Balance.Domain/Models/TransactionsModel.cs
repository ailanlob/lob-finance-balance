﻿using Lob.Finance.Balance.Domain.Interfaces;
using Lob.Finance.Balance.Domain.Utils;
using System.Text.Json.Serialization;

namespace Lob.Finance.Balance.Domain.Models
{
    public class TransactionsModel : IModel
    {
        /*
         * Base Model for Finance types:
         * 
         * Stock
         * Fixed income [PT: Renda Fixa]
         * Savings account [PT: Poupanca]
         * 
         */
        public virtual string Key => $"{Abbreviation}-{PurchaseDate.Replace("/", "")}-{(PricePaid == 0 ? 0.0 : PricePaid * Amount)}-{Seq}";

        [JsonConverter(typeof(IntDataConverter))]
        public int Seq { get; set; }

        public string Name { get; set; }

        public string Abbreviation { get; set; }

        [JsonConverter(typeof(DoubleDataConverter))]
        public double PricePaid { get; set; }

        public double MarketPrice { get; set; }

        public string Category { get; set; }

        [JsonConverter(typeof(IntDataConverter))]
        public int Amount { get; set; } = 1;

        public string PurchaseDate { get; set; }

        public double YieldValue { get; set; }

        public double YieldPercent { get; set; }
    }
}
