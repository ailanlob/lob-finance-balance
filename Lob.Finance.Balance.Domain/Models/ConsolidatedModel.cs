﻿using Lob.Finance.Balance.Domain.Interfaces;

namespace Lob.Finance.Balance.Domain.Models
{
    public class ConsolidatedModel : TransactionsModel, IModel
    {
        public override string Key => Abbreviation;
        public double TargetPercent { get; set; } = 0.0;
        public double CurrentPercent { get; set; } //{ get { return (Finance.Sum(x => x.MarketPrice) / AuxTotalInvested) * 100; } }
        public double DifferPercent { get; set; } //{ get { return TargetPercent - CurrentPercent; } }
        public string DifferPercentMarginStr { get; set; }//{ get { return (CurrentPercent - TargetPercent) > AcceptedMargin ? "OVER" : (CurrentPercent - TargetPercent) < (AcceptedMargin * -1) ? "BELOW" : "OK"; } }
        public double NeedsToBePurchaseAmount { get; set; } = 0;
    }
}
