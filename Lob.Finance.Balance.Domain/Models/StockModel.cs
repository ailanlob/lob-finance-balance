﻿using Lob.Finance.Balance.Domain.Interfaces;
using System;

namespace Lob.Finance.Balance.Domain.Models
{
    public class StockModel : IModel
    {
        public string Key => Name.ToLower();

        /// <summary>
        /// Stock name abreviation 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Stock current price
        /// </summary>
        public double Price { get; set; }

        //TODO: UpdatedDate received from firebase don't have a Kind, causing error in a insert of this value
        /// <summary>
        /// Last update datetime
        /// </summary>
        public DateTime UpdatedDate { get; set; } = DateTime.UtcNow;

        /// <summary>
        /// TradingView url to get price
        /// </summary>
        public string TradingView { get; set; }

        /// <summary>
        /// Daily Yield
        /// </summary>
        public double DailyYield { get; set; }

    }
}
