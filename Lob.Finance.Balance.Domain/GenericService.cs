﻿using Lob.Finance.Balance.Domain.Services;
using Lob.Finance.Balance.Infra.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lob.Finance.Balance.Domain
{
    public abstract class GenericService<T> : IService<T>
    {
        private IRepository<T> _repository;

        public GenericService(IRepository<T> repository)
        {
            _repository = repository;
        }

        public virtual async Task<T> Generate() => throw new System.NotImplementedException();

        public virtual async Task<List<T>> GenerateList() => throw new System.NotImplementedException();

        public async Task<T> Get()
        {
            var result = await _repository.Get();
            return result.FirstOrDefault();
        }

        public async Task<List<T>> GetList() => await _repository.Get();

        public bool Set(T model) => _repository.Set(model);
        public bool Set(List<T> model) => _repository.Set(model);
    }
}
