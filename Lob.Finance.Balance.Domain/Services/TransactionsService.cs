﻿using Lob.Finance.Balance.Domain.Models;
using Lob.Finance.Balance.Infra.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace Lob.Finance.Balance.Domain.Services
{
    public class TransactionsService : GenericServiceUser<TransactionsModel>, IService<TransactionsModel>
    {   
        public TransactionsService(
            IRepositoryUser<TransactionsModel> transactionsRepository,
            UserService userService) : base(userService, transactionsRepository)
        {
        }
    }
}
