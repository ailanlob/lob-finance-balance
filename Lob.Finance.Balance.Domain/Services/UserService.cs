﻿using Lob.Finance.Balance.Domain.Models;

namespace Lob.Finance.Balance.Domain.Services
{
    /// <summary>
    /// User to acess information in database
    /// This is unic for aplication to acess another services
    /// </summary>
    public class UserService
    {
        private UserModel _user;

        /// <summary>
        /// Sample login acess to create a user
        /// </summary>
        /// <param name="user"></param>
        /// <param name="pass"></param>
        public void Login(string user, string pass) => _user = new() { Name = user, Password = pass };

        /// <summary>
        /// Set the user to null
        /// </summary>
        public void Logout() => _user = null;

        /// <summary>
        /// Key used in acess repository information
        /// </summary>
        //TODO: Return a criptografed key
        //public string UserKey { get => _user.Name; }
        public string UserKey { get => _user is null ? "root" : _user.Name; }
    }
}
