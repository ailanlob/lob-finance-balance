﻿using Lob.Finance.Balance.Domain.Models;
using Lob.Finance.Balance.Infra.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lob.Finance.Balance.Domain.Services
{
    public class ConsolidatedService : GenericServiceUser<ConsolidatedModel>, IService<ConsolidatedModel>
    {
        private static IService<StockModel> _stockService;
        private static IService<TransactionsModel> _transactionsService;

        public ConsolidatedService(
            IRepositoryUser<ConsolidatedModel> consolidatedRepository,
            IService<StockModel> stockService,
            IService<TransactionsModel> transactionsService,
            UserService userService
            ) : base(userService, consolidatedRepository)
        {
            _stockService = stockService;
            _transactionsService = transactionsService;
        }

        public override async Task<List<ConsolidatedModel>> GenerateList()
        {
            List<ConsolidatedModel> _consolidatedFinances = new();

            List<TransactionsModel> transactions = await _transactionsService.GetList();
            List<StockModel> stocksRead = await _stockService.GetList();

            transactions.OrderBy(x => x.Abbreviation);

            InformationMining infoMining = new();
            foreach (TransactionsModel transaction in transactions.GroupBy(x => x.Abbreviation).Select(y => y.First()))
            {
                // TODO: Criar historico de preços de ações para caso de falha em pegar o atual pegar o ultimo obtido
                double price = stocksRead.FirstOrDefault(x => x.Name == transaction.Abbreviation).Price;

                if (price == 0)
                {
                    //TODO: [LOG] Error in get Value (Get now the price?)
                    Console.WriteLine("[ERROR] Get a zero price");
                }

                List<TransactionsModel> stockTransactions = transactions.Where(x => x.Abbreviation == transaction.Abbreviation).ToList();

                var totalAmount = stockTransactions.Sum(x => x.Amount);
                var mediaPrice = stockTransactions.Sum(x => x.PricePaid == 0 ? 0.0 : x.PricePaid * x.Amount) / totalAmount;
                ConsolidatedModel fBaseAux = new()
                {
                    Abbreviation = transaction.Abbreviation,
                    Amount = stockTransactions.Sum(x => x.Amount),
                    PricePaid = mediaPrice.Equals(double.NaN) ? 0.0 : mediaPrice,
                    MarketPrice = price,
                    Category = transaction.Category,
                    PurchaseDate = stockTransactions.LastOrDefault().PurchaseDate,
                    YieldValue = mediaPrice.Equals(double.NaN) ? 0.0 : (price * totalAmount) - (mediaPrice * totalAmount),
                    YieldPercent = ((mediaPrice.Equals(double.NaN) ? 0.0 : (price * totalAmount) - (mediaPrice * totalAmount)) / (mediaPrice * totalAmount)) * 100
                };
                _consolidatedFinances.Add(fBaseAux);
            }
            return _consolidatedFinances;
        }
    }
}
