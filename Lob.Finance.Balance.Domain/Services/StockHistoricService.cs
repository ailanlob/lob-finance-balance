﻿using Lob.Finance.Balance.Domain.Models;
using Lob.Finance.Balance.Infra.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lob.Finance.Balance.Domain.Services
{
    public class StockHistoricService : GenericService<StockHistoricModel>, IService<StockHistoricModel>
    {
        public StockHistoricService(
            IRepository<StockHistoricModel> stockHistoricRepository) : base(stockHistoricRepository) { }

        public async Task<double> GetStockHistoricPriceAsync(StockModel stock, DateTime dateStart)
        {
            string dateStartStr = dateStart.ToString("yyyy-MM-dd");

            List<StockHistoricModel> stocksReadToPrice = await GetList();
            StockHistoricModel stockReadToPrice = stocksReadToPrice.FirstOrDefault(x => x.UpdatedDate == dateStartStr);

            if (stocksReadToPrice is null)
            {
                if (string.IsNullOrEmpty(stock.TradingView))
                {
                    Console.WriteLine($"[ERROR] {stock.Name} need the TradingView");
                    return 0.0;
                }

                InformationMining infoMining = new();
                double price = stock.TradingView.Length > 0 ? await infoMining.GetStockPriceDate(stock.TradingView, dateStart) : 0.1;

                if (price > 0.0)
                {
                    List<StockHistoricModel> stockHistorics = new();
                    stockHistorics.Add(new StockHistoricModel()
                    {
                        Name = stock.Name,
                        Price = price,
                        UpdatedDate = dateStart.ToString("yyyy-MM-dd")
                    });
                    Set(stockHistorics);
                    return price;
                }
                else
                {
                    //GetFrom AlphaAPI
                }
            }

            return 0.0;
        }
    }
}
