﻿using Lob.Finance.Balance.Domain.Models;
using Lob.Finance.Balance.Infra.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lob.Finance.Balance.Domain.Services
{
    public class CategoryService : GenericServiceUser<CategoryModel>, IService<CategoryModel>
    {
        private static IService<ConfigCategoryModel> _configCategoryService;
        private static IService<ResumeModel> _resumeService;
        private static IService<ConsolidatedModel> _consolidatedService;

        public CategoryService(
            IRepositoryUser<CategoryModel> categoryRepository,
            UserService user,
            IService<ConfigCategoryModel> configCategoryService,
            IService<ResumeModel> resumeService,
            IService<ConsolidatedModel> consolidatedService) : base(user, categoryRepository) 
        {
            _configCategoryService = configCategoryService;
            _resumeService = resumeService;
            _consolidatedService = consolidatedService;
        }

        public override async Task<List<CategoryModel>> GenerateList()
        {
            List<CategoryModel> result = new();

            List<ConfigCategoryModel> configCategory = await _configCategoryService.GetList();
            List<ConsolidatedModel> consolidatedFinances = await _consolidatedService.GetList();
            ResumeModel resume = await _resumeService.Get();

            foreach (ConfigCategoryModel categ in configCategory)
            {
                List<ConsolidatedModel> categFinance = consolidatedFinances.Where(x => x.Category == categ.Name).ToList();
                #region auxValues
                double totalToInveste = resume.CurrentTotalValue + resume.NeedsToBeInvested;
                double auxTotalCateg = categFinance.Sum(x => x.MarketPrice == 0 ? 0.0 : x.MarketPrice * x.Amount);
                double auxCurrentPercent = (auxTotalCateg / totalToInveste) * 100;
                #endregion
                result.Add(new()
                {
                    Name = categ.Name,
                    TargetPercent = categ.TargetPercent,
                    TotalValue = auxTotalCateg,
                    CurrentPercent = auxCurrentPercent,
                    DifferPercent = categ.TargetPercent - auxCurrentPercent,
                    NeedsToBeInvested = ((categ.TargetPercent - auxCurrentPercent)) > 0 ?
                                              totalToInveste * ((categ.TargetPercent - auxCurrentPercent) / 100) :
                                              0.0,
                    YieldPercent = categFinance.Sum(x => x.YieldPercent),
                    YieldValue = categFinance.Sum(x => x.YieldValue)
                });
            }
            result = result.OrderByDescending(x => x.TargetPercent).ToList();

            return result;
        }
    }
}
