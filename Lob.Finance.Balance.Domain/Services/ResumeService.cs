﻿using Lob.Finance.Balance.Domain.Models;
using Lob.Finance.Balance.Infra.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lob.Finance.Balance.Domain.Services
{
    public class ResumeService : GenericService<ResumeModel>, IService<ResumeModel>
    {
        private static IService<TransactionsModel> _transactionService;
        private static IService<ConsolidatedModel> _consolidatedService;
        private static UserService _userService;


        public ResumeService(
            IRepository<ResumeModel> resumeRepository,
            IService<TransactionsModel> transactionService,
            IService<ConsolidatedModel> consolidatedService,
            UserService userService
            ) : base(resumeRepository) 
        {
            _transactionService = transactionService;
            _consolidatedService = consolidatedService;
            _userService = userService;
        }

        public override async Task<ResumeModel> Generate()
        {
            List<TransactionsModel> transactions = await _transactionService.GetList();
            List<ConsolidatedModel> consolidated = await _consolidatedService.GetList();

            double _totalInvested = transactions.Sum(x => x?.PricePaid == 0 ? 0.0 : x.PricePaid * x.Amount);
            double _totalMarketValue = consolidated.Sum(x => x?.MarketPrice == 0 ? 0.0 : x.MarketPrice * x.Amount);

            return new()
            {
                User = _userService.UserKey,
                TotalInvestedValue = _totalInvested,
                NeedsToBeInvested = 0.0,
                CurrentTotalValue = _totalMarketValue,
                YieldValue = _totalMarketValue - _totalInvested,
                YieldPercent = ((_totalMarketValue - _totalInvested) / _totalInvested) * 100
            };

        }
    }
}
