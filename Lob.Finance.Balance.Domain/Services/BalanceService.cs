﻿using Lob.Finance.Balance.Domain.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lob.Finance.Balance.Domain.Services
{
    public class BalanceService
    {
        //TODO: Create Interface
        private static IService<CategoryModel> _categoryService;
        private static IService<ConsolidatedModel> _consolidatedService;
        private static IService<ResumeModel> _resumeService;

        private List<ConsolidatedModel> _consolidatedFinances;

        public BalanceService(
            IService<CategoryModel> categoryService,
            IService<ConsolidatedModel> consolidatedService,
            IService<ResumeModel> resumeService)
        {
            _categoryService = categoryService;
            _consolidatedService = consolidatedService;
            _resumeService = resumeService;
        }

        public async Task<List<ConsolidatedModel>> StockValueToBeInvested(bool noInvestedFirst = true)
        {
            //Recreate the function to distribute the disponibleValue to all category
            //OR
            //[X]Get the most differPercent Category First
            //OR
            //Get the most percent target category
            //OR
            //Get the most Rating stock independet of category

            var financeCategory = await _categoryService.GetList();
            _consolidatedFinances = await _consolidatedService.GetList();
            var resume = await _resumeService.Get();

            if (noInvestedFirst)
                InvestingInNoPositionStockFirst();


            double avaliableValue = resume.NeedsToBeInvested;
            _ = financeCategory.OrderBy(x => x.NeedsToBeInvested);
            foreach (CategoryModel category in financeCategory)
            {
                double categoryAvaliableValue = category.NeedsToBeInvested < avaliableValue ? category.NeedsToBeInvested : avaliableValue;
                double totalPurchaseValue = 0.0;
                List<ConsolidatedModel> transactionsConsolided = _consolidatedFinances.Where(x => x.Category == category.Name).ToList();
                if (categoryAvaliableValue > 0)
                {
                    bool lockWhile = false;
                    while (!lockWhile)
                    {
                        ConsolidatedModel lowerMarketPrice = transactionsConsolided.OrderBy(x => x.MarketPrice).FirstOrDefault();
                        lockWhile = categoryAvaliableValue <= lowerMarketPrice.MarketPrice;
                        if (lockWhile) break;
                        //TODO: Order by stock rating and determine the target percent to the stock
                        foreach (ConsolidatedModel financeBase in transactionsConsolided)
                        {
                            if (categoryAvaliableValue - financeBase.MarketPrice > 0)
                            {
                                categoryAvaliableValue -= financeBase.MarketPrice;
                                totalPurchaseValue += financeBase.MarketPrice;
                                financeBase.NeedsToBePurchaseAmount++;
                            }
                        }
                    }
                }
                avaliableValue -= totalPurchaseValue;
                //finance.AuxTotalInvested = _listFinanceByCategory.Sum(x => x.TotalValue);
                //?? category.AuxTotalInvested = _totalInvested;
            }
            return _consolidatedFinances;
        }

        public List<ConsolidatedModel> ResetStockValueToBeInvested()
        {
            foreach (ConsolidatedModel finance in _consolidatedFinances)
            {
                finance.NeedsToBePurchaseAmount = 0;
            }
            return _consolidatedFinances;
        }

        public void InvestingInNoPositionStockFirst()
        {
            foreach (ConsolidatedModel financeBase in _consolidatedFinances)
            {
                financeBase.NeedsToBePurchaseAmount = financeBase.PricePaid == 0 && financeBase.NeedsToBePurchaseAmount == 0 ? 1 : 0;
            }
        }
    }
}
