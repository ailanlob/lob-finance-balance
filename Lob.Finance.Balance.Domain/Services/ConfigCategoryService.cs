﻿using Lob.Finance.Balance.Domain.Models;
using Lob.Finance.Balance.Infra.Interfaces;
using Lob.Finance.Balance.Infra.Repositories;
using Lob.Finance.Balance.Infra.Utils;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace Lob.Finance.Balance.Domain.Services
{
    public class ConfigCategoryService : GenericServiceUser<ConfigCategoryModel>, IService<ConfigCategoryModel>
    {
        public ConfigCategoryService(
            IRepositoryUser<ConfigCategoryModel> configCategoryRepository,
            UserService userService
            ) : base(userService, configCategoryRepository) { }
    }
}
