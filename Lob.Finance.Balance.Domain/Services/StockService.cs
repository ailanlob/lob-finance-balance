﻿using Lob.Finance.Balance.Domain.Models;
using Lob.Finance.Balance.Infra.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lob.Finance.Balance.Domain.Services
{
    public class StockService : GenericService<StockModel>, IService<StockModel>
    {
        IService<TransactionsModel> _transactionService;

        public StockService(
            IRepository<StockModel> stockRepository, 
            IService<TransactionsModel> transactionService) : base(stockRepository) 
        {
            _transactionService = transactionService;
        }

        public async Task<bool> InsertNewsFromTransactionsAsync()
        {
            List<StockModel> stocksRead = await GetList();
            List<TransactionsModel> financeTransactions = await _transactionService.GetList();

            financeTransactions.OrderBy(x => x.Abbreviation);

            List<StockModel> stocks = new();
            foreach (TransactionsModel fBase in financeTransactions.GroupBy(x => x.Abbreviation).Select(y => y.First()))
                if (!stocksRead.Any(x => x.Name == fBase.Abbreviation))
                    stocks.Add(new StockModel() { Name = fBase.Abbreviation });

            Set(stocks);

            return stocks.Count > 0;
        }
    }
}
