﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Lob.Finance.Balance.Domain.Utils
{
    public class IntDataConverter : JsonConverter<int>
    {
        public override int Read(ref Utf8JsonReader reader, 
                                Type typeToConvert, 
                                JsonSerializerOptions options) => int.Parse(reader.GetString());

        public override void Write(Utf8JsonWriter writer, int value, 
                                    JsonSerializerOptions options) => writer.WriteStringValue(value.ToString());
    }
}
